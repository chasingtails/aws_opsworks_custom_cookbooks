default[:data_set_importation_configs][:nginx_dir] = "/etc/nginx"
default[:data_set_importation_configs][:application_directory] = "/srv/www/data_set_importation_service/current"
default[:data_set_importation_configs][:nginx_log_dir] = "/var/log/nginx" 