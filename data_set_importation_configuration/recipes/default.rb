Chef::Log.info("Generating Nginx site template for data_set_importation_service")
template "#{node[:data_set_importation_configs][:nginx_dir]}/sites-available/default" do
  source "default-site.erb"
  owner "root"
  group "root"
  mode 0644
end

service "nginx" do
  action [ :enable, :restart ]
end

Chef::Log.info("Generating upstart config for data_set_importation_service")
template "python_app.upstart.conf" do
  path "/etc/init/python_app.conf"
  source "python-app.upstart.conf.erb"
  owner "root"
  group "root"
  mode "0644"
end