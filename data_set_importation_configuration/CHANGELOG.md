data_set_importation_configuration CHANGELOG
============================================

This file is used to list changes made in each version of the data_set_importation_configuration cookbook.

0.1.0
-----
- [cluency] - Initial release of data_set_importation_configuration