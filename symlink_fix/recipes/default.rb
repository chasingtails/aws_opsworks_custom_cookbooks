node[:deploy].each do |application, deploy|

	bash "fix broken symlinking detection" do
	  cwd "#{deploy[:current_path]}/config"
	  problematic_files = %w(database.yml local_env.yml)
	  problematic_files.each do |problematic_file|
		  code <<-EOC
		       sudo cp #{problematic_file} _#{problematic_file}
		       sudo mv _#{problematic_file} #{problematic_file}
		       sudo chown deploy:www-data #{problematic_file}
		       sudo chmod 066 #{problematic_file}
		       echo "Fix for #{problematic_file}"
		    EOC
	  end
	  
	  only_if do
        File.exists?("#{deploy[:deploy_to]}") && File.exists?("#{deploy[:deploy_to]}/current/config/")
    end
	end

end