node[:deploy].each do |application, deploy|
	if deploy["custom_type"] != 'python'
    next
  end

	bash "symlink application.yml" do
	  cwd "#{deploy[:current_path]}/config"
	  config_file = "application.yml"
	  code <<-EOC
	       sudo ln -srv ../../../shared/config/application.yml application.yml
	    EOC
	  
	  only_if do
        File.exists?("#{deploy[:deploy_to]}") && File.exists?("#{deploy[:deploy_to]}/current/config/")
    end
	end

end