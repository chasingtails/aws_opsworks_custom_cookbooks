node[:deploy].each do |application, deploy|
	if deploy["custom_type"] != 'python'
    next
  end

	bash "install packages from requirements.txt using pip and restart server" do
		cwd "#{deploy[:current_path]}"
		code <<-EOC
					sudo -H pip install -r requirements.txt
					sudo restart python_app
			EOC
	end
end