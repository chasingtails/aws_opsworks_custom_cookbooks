name             'data_set_importation_post_deploy'
maintainer       'Cluency'
license          'All rights reserved'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
