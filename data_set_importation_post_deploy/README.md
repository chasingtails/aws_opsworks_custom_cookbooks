Data Set Importation Service Post Deploy Cookbook
===================================================

This cookbook performs automated tasks such as:
		- Restarting the application once a deploy is finished.
		- Installing packages from the apps requirements.txt using pip.