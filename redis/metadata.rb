name             "redis"
maintainer       "Julian Tescher"
maintainer_email "jatescher@gmail.com"
license          "MIT"
description      "Installs/Configures redis"
version          "0.1.0"

depends          'build-essential', '>= 1.4.0'
